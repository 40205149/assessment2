﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/*
 * Author: Michael Paxton (40205149)
 * Class Purpose: Store information and methods for guests
 * Last augmented: 01/12/16
 * Design Patterns: None.
 */

namespace _40205149.cw2
{
    public class guest
    {
        //Private properties
        private string _fstname, _sndname, _passportnum;
        private int _age, _bookingrefnum, _custrefnum, _guestrefnum;

        //public properties       
        public string FstName
        {
            get { return _fstname; }
            set
            {
                try
                {
                    if (value.Trim() == "" || value.Trim() == null)
                        throw new ArgumentException("**ERROR: Please enter a first name.**");
                    _fstname = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public string SndName
        {
            get { return _sndname; }
            set
            {
                try
                {
                    if (value.Trim() == "" || value.Trim() == null)
                        throw new ArgumentException("**ERROR: Please enter a surname.**");
                    _sndname = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public string PassPortNum
        {
            get { return _passportnum; }
            set 
            {
                try
                {
                    if (value.Length > 10)
                        throw new ArgumentException("**ERROR: Please enter a valid passport number.**");
                    if (value == "" || value == null)
                        throw new ArgumentException("**ERROR: Please enter a passport number.**");
                    _passportnum = value; 
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public int Age
        {
            get { return _age; }
            set 
            { 
                try
                {
                    if (value > 101)
                        throw new ArgumentException("**ERROR: Please enter a lower age.**");
                    if (value < 0)
                        throw new ArgumentException("**ERROR: Please enter a higher age.**");
                    if (value == 0)
                        throw new ArgumentException("**ERROR: Please enter an age.**");
                    _age = value; 
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }                
            }
        }

        public int CustomerRefNum
                {
                    get { return _custrefnum; }
                    set { _custrefnum = value; }
                }

        public int BookingRefNum
        {
            get { return _bookingrefnum; }
            set { _bookingrefnum = value; }
        }

        public int GuestRefNum
        {
            get { return _guestrefnum; }
            set { _guestrefnum = value; }
        }

        //Methods
        public string displayGuest()
        {
            return ("Customer: " + _custrefnum + ". Booking: " + _bookingrefnum + ". Guest: " + _fstname + " " + SndName + ". Has the passport number of: " + _passportnum + ". Is the age of: " + _age + ".");
        }

    }
}
