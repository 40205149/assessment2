﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace _40205149.cw2
{
    /*
     * Author: Michael Paxton (40205149)
     * Class Purpose: storing information and methods for car hires 
     * Last augmented: 08/12/16
     * Design Patterns: None
     */
    public class CarHireInfo
    {
        //private properties
        private DateTime _startdate = new DateTime();
        private DateTime _enddate = new DateTime();
        private string _fstname, _surname;
        private int _bookingref;

        //public properties
        public DateTime StartDate
        {
            get { return _startdate; }
            set
            {
                try
                {
                    if (value < DateTime.Today)
                        throw new ArgumentException("**ERROR: Please enter a valid date**");
                    _startdate = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public DateTime EndDate
        {
            get { return _enddate; }
            set
            {
                try
                {
                    if (value < DateTime.Today)
                        throw new ArgumentException("**ERROR: Please enter a valid date**");
                    if (value <= _startdate)
                        throw new ArgumentException("**ERROR: Please enter an end date later than the arrival date**");
                    _enddate = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public string FstName
        {
            get { return _fstname; }
            set
            {
                try
                {
                    if (value.Trim() == "" || value.Trim() == null)
                        throw new ArgumentException("**ERROR: Please enter a first name.**");
                    _fstname = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public string Surname
        {
            get { return _surname; }
            set
            {
                try
                {
                    if (value.Trim() == "" || value.Trim() == null)
                        throw new ArgumentException("**ERROR: Please enter a surname.**");
                    _surname = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public int bookingRef
        {
            get { return _bookingref; }
            set { _bookingref = value; }
        }

        //public methods
        public double getDays(DateTime Strt, DateTime End)
        {
            return (End - Strt).TotalDays;
        }

        public DateTime datetovar(string dat)
        {
            if (dat.Equals(""))
            {
                return new DateTime();
            }
            else
            {
                return new DateTime(Int32.Parse(dat.Substring(6, 4)),
                    Int32.Parse(dat.Substring(3, 2)),
                    Int32.Parse(dat.Substring(0, 2)));
            }
        }


    }
}