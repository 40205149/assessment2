﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /*
     * Author: Michael Paxton (40205149)
     * Class Purpose: Main method of program, accesses all functions within the program
     * Last augmented: 09/12/16
     * Design Patterns: None
     */

    public partial class MainWindow : Window
    {
        //instance of custrefnumsingleton and refnumsingleton   
        custrefnumsingleton custnum = custrefnumsingleton.custInstance;
        refnumsingleton refnum = refnumsingleton.refInstance;

        //pulbic lists of type customer, booking, extras, carhireinfo and guest class.
        public static List<customer> cstl = new List<customer>();
        public static List<booking> bkl = new List<booking>();
        public static List<extras> extrl = new List<extras>();  
        public static List<CarHireInfo> chil = new List<CarHireInfo>();
        public static List<guest> gstl = new List<guest>();       
       
        public MainWindow()
        {          
            InitializeComponent();
        }

        //used to create a new customer
        private void btnNewCust_Click(object sender, RoutedEventArgs e)
        {
            NewCust newCustInst = new NewCust();
            newCustInst.Show();
            Close();
        }

        //used to view existing customers
        private void btnExistingCust_Click(object sender, RoutedEventArgs e)
        {
            bool haschecked = false;
            //if string can be parsed then show booking details, else if text box is null, empty or not an integer then error message
            if (txtCustToView.Text != null && txtCustToView.Text != "")
            {
                int customertoview;
                if (int.TryParse(txtCustToView.Text, out customertoview) == true)
                    for (int i = 0; i < cstl.Count; i++)
                    {
                        if (cstl[i].CustRefNum == customertoview)
                        {
                            haschecked = true;
                            int custrefnum = cstl[i].CustRefNum;
                            string foreName = cstl[i].FstName;
                            string surname = cstl[i].SndName;
                            string address = cstl[i].Address;
                                
                            //open up a viewcust window with edit options.
                            NewCust viewCustInst = new NewCust(custrefnum, foreName, surname, address);
                            viewCustInst.Show();
                            Close();
                        }
                    }
            }
            if (haschecked == false)
                MessageBox.Show("**ERROR: Please input a reference to a booking that exists**");
        }

        //used to view existing bookings
        private void btnExistingBooking_Click(object sender, RoutedEventArgs e)
        {
            bool haschecked = false;
            //if string can be parsed then show booking details, else if text box is null, empty or not an integer then error message
            if (txtBookingToView.Text != null && txtBookingToView.Text != "")
            {
                int bookingtoview;
                if (int.TryParse(txtBookingToView.Text, out bookingtoview) == true)
                    foreach (booking bk in MainWindow.bkl)
                    {
                        //if current booking's refnum is equal to the one we want to view then load the values
                        if (bk.RefNum == bookingtoview)
                        {
                            haschecked = true;
                            DateTime arrivalDate = bk.ArrivalDate;
                            DateTime departureDate = bk.DepartureDate;
                            int numOfGuests = bk.NoOfGuests;
                            int bkIndex = bk.RefNum;

                            //open up a viewcust window with edit options.
                            NewBooking viewBookingInst = new NewBooking(bkIndex, arrivalDate, departureDate, numOfGuests);
                            viewBookingInst.Show();
                            Close();
                        }                        
                    }
            }
            if (haschecked == false)
                MessageBox.Show("**ERROR: Please input a reference to a booking that exists**");
        }

        //used to view existing extras
        private void btnExistingExtras_Click(object sender, RoutedEventArgs e)
        {
            bool haschecked = false;
            //if string can be parsed then show booking details, else if text box is null, empty or not an integer then error message
            if (txtBookingToView.Text != null && txtBookingToView.Text != "")
            {
                int extrastoview;
                if (int.TryParse(txtBookingToView.Text, out extrastoview) == true)
                {
                    for (int i = 0; i < bkl.Count; i++)
                    {
                        //if current booking's refnum is equal to the one we want to view then load the values
                        if (extrl[i].bookingRef == extrastoview)
                        {
                            bool eveMeal = false;
                            bool breakfast = false;
                            bool carhire = false;
                            string eveMealInfo = "";
                            string breakfastInfo = "";
                            DateTime startDate = new DateTime();
                            DateTime endDate = new DateTime();
                            string forename = "";
                            string surname = "";
                            int bkRefNum = extrl[i].bookingRef;

                            haschecked = true;
                            if (extrl[i].eveMeal == true)
                            {
                                eveMeal = true;
                                eveMealInfo = extrl[i].eveMealInfo;
                            }
                            else if (extrl[i].eveMeal == false)
                            {
                                eveMeal = false;
                                eveMealInfo = "";
                            }
                            if (extrl[i].breakfast == true)
                            {
                                breakfast = true;
                                breakfastInfo = extrl[i].breakfastInfo;
                            }
                            else if (extrl[i].breakfast == false)
                            {
                                breakfast = false;
                                breakfastInfo = "";
                            }
                            if (extrl[i].carHire == true)
                            {
                                carhire = true;
                                startDate = chil[i].StartDate;
                                endDate = chil[i].EndDate;
                                forename = chil[i].FstName;
                                surname = chil[i].Surname;
                            }                                
                            else if (extrl[i].carHire == false)
                            {
                                carhire = false;
                            }

                            //open up a viewcust window with edit options.
                            NewExtras viewExtrasInst = new NewExtras(bkRefNum, eveMeal, eveMealInfo, breakfast, breakfastInfo, carhire);
                            viewExtrasInst.Show();
                            Close();
                        }              
                    }
                    
                } 
            }
            if (haschecked == false)
            {
                MessageBox.Show("**ERROR: Please input a reference to a list of extras that exists**");
            }
         }

        private void btnExistingGuest_Click(object sender, RoutedEventArgs e)
        {
            //create some variables
            bool haschecked = false;
            int[] numsToPass = {0,0};

            //if string can be parsed then show booking details, else if text box is null, empty or not an integer then error message
            if (txtGuestToView.Text != null && txtGuestToView.Text != "")
            {

                //turn bktoview into an int
                int bktoview;
                if (int.TryParse(txtBkForGuest.Text, out bktoview) == true)
                {
                    //turn txttoview into an int
                    int guesttoview;
                    if (int.TryParse(txtGuestToView.Text, out guesttoview) == true)
                    {
                        guesttoview = guesttoview - 1;
                        for (int i = 0; i < gstl.Count; i++)
                        {
                            if (gstl[i].BookingRefNum == bktoview && gstl[i].GuestRefNum == guesttoview)
                            {
                                haschecked = true;
                                //fill tools on GUI with customer properties                              
                                numsToPass[0] = gstl[i].BookingRefNum;
                                numsToPass[1] = gstl[i].GuestRefNum;

                                NewGuest viewGuestInst = new NewGuest(numsToPass);
                                viewGuestInst.Show();
                                Close();
                            }
                        }
                    }
                }               
            }
            if (haschecked == false)
                MessageBox.Show("**ERROR: Please input a reference to a booking and guest that exist.**");
        }

        private void btnTestGuest_Click(object sender, RoutedEventArgs e)
        {
            foreach (guest gst in gstl)
            {
                MessageBox.Show("" + gst.displayGuest() );
            }
        }

        private void btnWriteToFile_Click(object sender, RoutedEventArgs e)
        {
            //create instance of streamwriter
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"C:\Users\Michael\Dropbox\University\Year 2\Tr 1\Software Development 2\test.txt", true))
            {
                file.WriteLine("Contents of Customer List:");
                file.WriteLine("");
                foreach (customer cst in cstl)
                {
                    file.WriteLine("" + cst.CustRefNum + "." + cst.FstName + "." + cst.SndName + "." + cst.Address);
                }
                file.WriteLine("");
                file.WriteLine("");
                MessageBox.Show("**Contents of customer list written to file**");

                file.WriteLine("Contents of Booking List:");
                file.WriteLine("");
                foreach (booking bk in bkl)
                {
                    file.WriteLine("" + bk.CustomerRefNum + "." + bk.RefNum + "." + bk.ArrivalDate.ToString() + "." + bk.DepartureDate.ToString() + "." + bk.NoOfGuests.ToString());
                }
                file.WriteLine("");
                file.WriteLine("");
                MessageBox.Show("**Contents of booking list written to file**");

                file.WriteLine("Contents of Extras List:");
                file.WriteLine("");
                foreach (extras ext in extrl)
                {
                    file.WriteLine("" + ext.bookingRef + "." + ext.eveMeal + "." + ext.eveMealInfo + "." + ext.breakfast + "." + ext.breakfastInfo + "." + ext.carHire);
                }
                file.WriteLine("");
                file.WriteLine("");
                MessageBox.Show("**Contents of extras list written to file**");

                file.WriteLine("Contents of Car Hire Info List:");
                file.WriteLine("");
                foreach (CarHireInfo chi in chil )
                {
                    file.WriteLine("" + chi.bookingRef + "." + chi.FstName + "." + chi.Surname + "." + chi.StartDate + "." + chi.EndDate);
                }
                file.WriteLine("");
                file.WriteLine("");
                MessageBox.Show("**Contents of car hire info list written to file**");

                file.WriteLine("Contents of Guest List:");
                file.WriteLine("");
                foreach (guest gst in gstl)
                {
                    file.WriteLine("" + gst.CustomerRefNum + "." + gst.BookingRefNum + "." + gst.GuestRefNum + "." + gst.FstName + "." + gst.SndName + "." + gst.PassPortNum + "." + gst.Age);
                }
                MessageBox.Show("**Contents of guest list written to file**");
                
            }           
        }

        private void btnReadFromFile_Click(object sender, RoutedEventArgs e)
        {
            //store each line in an array of strings
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Michael\Dropbox\University\Year 2\Tr 1\Software Development 2\test.txt");
            string datatype = "";

            // Display the file contents by using a foreach loop.

            foreach (string line in lines)
            {
                if (line.Equals("Contents of Customer List:"))
                    datatype = "cst";

                if (datatype.Equals("cst") && !line.Equals("") && !line.Equals("Contents of Customer List:") && !line.Equals("Contents of Booking List:"))
                {
                    //create new instance of customer
                    customer cst1 = new customer();

                    //split the line at every full stop into an array of strings
                    string[] words = line.Split('.');
                    int cstrefnum = Int32.Parse(words[0]);
                    custnum.CustRefNum = Int32.Parse(words[0]);
                    cst1.CustRefNum = cstrefnum;
                    cst1.FstName = words[1];
                    cst1.SndName = words[2];
                    cst1.Address = words[3];

                    //add this instance of customer to the customer list
                    cstl.Add(cst1);
                }

                if (line.Equals("Contents of Booking List:"))
                    datatype = "bk";

                if (datatype.Equals("bk") && !line.Equals("") && !line.Equals("Contents of Booking List:") && !line.Equals("Contents of Extras List:"))
                {
                    //create new instance of booking
                    booking bk1 = new booking();

                    //split the line at every full stop into an array of strings
                    string[] words = line.Split('.');

                    //fill properties of booking
                    bk1.CustomerRefNum = Int32.Parse(words[0]);
                    bk1.RefNum = Int32.Parse(words[1]);
                    refnum.CurRefNum = Int32.Parse(words[1]);
                    bk1.ArrivalDate = bk1.datetovar(words[2]);
                    bk1.DepartureDate = bk1.datetovar(words[3]);
                    bk1.NoOfGuests = Int32.Parse(words[4]);

                    //add booking to the booking list
                    bkl.Add(bk1);

                }

                if (line.Equals("Contents of Extras List:"))
                    datatype = "extr";

                if (datatype.Equals("extr") && !line.Equals("") && !line.Equals("Contents of Extras List:") && !line.Equals("Contents of Car Hire Info List:"))
                {
                    //create new instance of extras
                    extras extr1 = new extras();

                    //split the line at every full stop into an array of strings
                    string[] words = line.Split('.');

                    //fill properties of extras
                    extr1.bookingRef = Int32.Parse(words[0]);
                    if (words[1].Equals("True"))
                        extr1.eveMeal = true;
                    else
                        extr1.eveMeal = false;
                    extr1.eveMealInfo = words[2];
                    if (words[3].Equals("True"))
                        extr1.breakfast = true;
                    else
                        extr1.breakfast = false;
                    extr1.breakfastInfo = words[4];
                    if (words[5].Equals("True"))
                        extr1.carHire = true;
                    else
                        extr1.carHire = false;

                    //add extras to extras list
                    extrl.Add(extr1);
                }

                if (line.Equals("Contents of Car Hire Info List:"))
                    datatype = "chi";

                if (datatype.Equals("chi") && !line.Equals("") && !line.Equals("Contents of Car Hire Info List:") && !line.Equals("Contents of Guest List:"))
                {
                    //create new instance of CarHireInfo
                    CarHireInfo chi1 = new CarHireInfo();

                    //split the line at every full stop into an array of strings
                    string[] words = line.Split('.');

                    //fill properties of guest
                    chi1.bookingRef = Int32.Parse(words[0]);
                    chi1.FstName = words[1];
                    chi1.Surname = words[2];
                    chi1.StartDate = chi1.datetovar(words[3]);
                    chi1.EndDate = chi1.datetovar(words[4]);

                    //add car hire info to list
                    chil.Add(chi1);
                }

                if (line.Equals("Contents of Guest List:"))
                    datatype = "gst";

                if (datatype.Equals("gst") && !line.Equals("") && !line.Equals("Contents of Guest List:"))
                {
                    //create new instance of guest
                    guest gst1 = new guest();

                    //split the line at every full stop into an array of strings
                    string[] words = line.Split('.');

                    //fill properties of guest
                    gst1.CustomerRefNum = Int32.Parse(words[0]);
                    gst1.BookingRefNum = Int32.Parse(words[1]);
                    gst1.GuestRefNum = Int32.Parse(words[2]);
                    gst1.FstName = words[3];
                    gst1.SndName = words[4];
                    gst1.PassPortNum = words[5];
                    gst1.Age = Int32.Parse(words[6]);

                    //add guest to guest list
                    gstl.Add(gst1);
                }
            }
            MessageBox.Show("**Loading from file complete**");
        }        
          
    }
}