﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /// <summary>
    /// Interaction logic for NewBooking.xaml
    /// </summary>
    public partial class NewBooking : Window
    {
        public NewBooking()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            booking bk1 = new booking();
            bookinglist bkl1 = new bookinglist(); 
            refnumsingleton refnum = refnumsingleton.Instance;

            //filling the properties of booking
            bk1.ArrivalDate = new DateTime(Int32.Parse(dtpArrival.SelectedDate.ToString().Substring(6, 4)),
                Int32.Parse(dtpArrival.SelectedDate.ToString().Substring(3, 2)),
                Int32.Parse(dtpArrival.SelectedDate.ToString().Substring(0, 2)));
            bk1.DepartureDate = new DateTime(Int32.Parse(dtpDeparture.SelectedDate.ToString().Substring(6, 4)),
                Int32.Parse(dtpDeparture.SelectedDate.ToString().Substring(3, 2)),
                Int32.Parse(dtpDeparture.SelectedDate.ToString().Substring(0, 2)));
            bk1.RefNum = refnum.getNextRefNum();

            //add current booking to list
            bkl1.NewList.Add(bk1);

            //testing
            for (int i = 0; i < bkl1.NewList.Count; i++)
            {
                MessageBox.Show("" + bkl1.NewList[i].displayBooking());
            }
        }
    }
}
