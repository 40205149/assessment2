﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40205149.cw2
{
    class customer
    {
        private string _name;
        private string _address;
        private int _custrefnum;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        
        public int CustRefNum
        {
            get { return _custrefnum; }
            set { _custrefnum = value; }
        }

    }
}
