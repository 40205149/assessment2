﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace _40205149.cw2
{
    class booking
    {
        //Private Properties
        private DateTime _arrivaldate = new DateTime();
        private DateTime _departuredate = new DateTime();
        private int _refnum;     
        
        refnumsingleton refinstance = refnumsingleton.Instance;

        //Public Properties
        public DateTime ArrivalDate
        {
            get { return _arrivaldate; }
            set { _arrivaldate = value; }
        }

        public DateTime DepartureDate
        {
            get { return _departuredate; }
            set { _departuredate = value; }
        }

        public int RefNum
        {
            get { return _refnum; }
            set { _refnum = value; }
        }

        //Methods

        public string displayBooking()
        {
            string output = "Arrival Date: " + _arrivaldate + " Departure Date: " + _departuredate + " Reference Number: " + _refnum;
            return output;
        }

        
    }
}
