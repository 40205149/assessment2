﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //instance of booking and refnumsingleton
        booking bk1 = new booking();
        refnumsingleton refnum = refnumsingleton.Instance;
        //list of type booking class
        List<booking> bkl = new List<booking>();
        

        public MainWindow()
        {          
            InitializeComponent();
        }

        private void btnNewBooking_Click(object sender, RoutedEventArgs e)
        {
            booking bk1 = new booking();
            

            //filling the properties of booking
            bk1.ArrivalDate = new DateTime(Int32.Parse(dtpArrival.SelectedDate.ToString().Substring(6, 4)),
                Int32.Parse(dtpArrival.SelectedDate.ToString().Substring(3, 2)),
                Int32.Parse(dtpArrival.SelectedDate.ToString().Substring(0, 2)));
            bk1.DepartureDate = new DateTime(Int32.Parse(dtpDeparture.SelectedDate.ToString().Substring(6, 4)),
                Int32.Parse(dtpDeparture.SelectedDate.ToString().Substring(3, 2)),
                Int32.Parse(dtpDeparture.SelectedDate.ToString().Substring(0, 2)));
            bk1.RefNum = refnum.getNextRefNum();

            //add current booking to list
            bkl.Add(bk1);        

            //testing 
            MessageBox.Show("" + bk1.displayBooking());
        }

        private void btnShowBooking_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < bkl.Count; i++)
            {
                MessageBox.Show("" + bkl[i].displayBooking());
            }
        }

    }
}
