﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40205149.cw2
{
    public class refnumsingleton
    {
        //declare an instance of refnumsingleton
        private static refnumsingleton instance;
        private refnumsingleton() { }

        //create an instance of refnumsingleton
        public static refnumsingleton Instance
        {
            get
            {
                // If being called for the first time, create refnum object.
                if (instance == null)                                 
                {
                    instance = new refnumsingleton();
                }
                return instance;
            }
        }

        //properties
        private int _currefnum = 500000;
        public int CurRefNum 
        {
            get { return _currefnum; }
            set { _currefnum = value; }
        }

        //Methods
        public int getNextRefNum()
        {
            _currefnum = _currefnum + 1;
            return _currefnum;
        }

    }
}

        

            

        

 
