﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /*
     * Author: Michael Paxton (40205149)
     * Class Purpose: Used for inputting, amending and deleting information about new guests
     * Last augmented: 08/12/16
     * Design Patterns: None
     */
    public partial class NewGuest : Window
    {       
        custrefnumsingleton custnum = custrefnumsingleton.custInstance;
        refnumsingleton refnum = refnumsingleton.refInstance;

        //used for inputting a new guest
        public NewGuest()
        {
            InitializeComponent();           
            
            //display appropriate tools
            btnConfirm.Visibility = Visibility.Visible;
            btnNewGuest.Visibility = Visibility.Hidden;
            btnApplyChanges.Visibility = Visibility.Hidden;
            btnDeleteGuest.Visibility = Visibility.Hidden;
            lblGuestToView.Visibility = Visibility.Hidden;          
        }

        

        //used for viewing an existing guest
        public NewGuest(int[] neededNums)
        {
            InitializeComponent();

            //disply appropriate tools
            btnNewGuest.Visibility = Visibility.Hidden;
            btnConfirm.Visibility = Visibility.Hidden;
            btnApplyChanges.Visibility = Visibility.Visible;
            btnDeleteGuest.Visibility = Visibility.Visible;
            lblGuestToView.Visibility = Visibility.Visible;

            //load tools with properties of guest
            lblBkToView.Content = (neededNums[0]).ToString();
            lblGuestToView.Content = (neededNums[1] + 1).ToString();
            
            foreach (guest gst in MainWindow.gstl)
            {
                if (gst.BookingRefNum == neededNums[0] && gst.GuestRefNum == neededNums[1] )
                {
                    txtForeName.Text = gst.FstName;
                    txtSurname.Text = gst.SndName;
                    txtPPN.Text = gst.PassPortNum;
                    txtAge.Text = gst.Age.ToString();

                    MessageBox.Show("**Guest loaded**");
                }
            }

        }

        int nNumOfGuests;
        int cNumOfGuests;
        int guestsLeft;
        //used for adding an extra guest onto a booking
        public NewGuest(int curNumOfGuests, int newNumOfGuests)
        {
            InitializeComponent();

            //display appropriate tools
            btnNewGuest.Visibility = Visibility.Visible;
            btnConfirm.Visibility = Visibility.Hidden;
            btnApplyChanges.Visibility = Visibility.Hidden;
            btnDeleteGuest.Visibility = Visibility.Hidden;
            lblGuestToView.Visibility = Visibility.Hidden;

            nNumOfGuests = newNumOfGuests;
            cNumOfGuests = curNumOfGuests;
            guestsLeft = nNumOfGuests - cNumOfGuests;
        }

        int guestAcc = 0;
        //used for creating a new guest
        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {         
            int currentrefnum = refnum.CurRefNum;

            if (guestAcc < MainWindow.bkl[currentrefnum - 1].NoOfGuests)
            {
                //declaring instance of guest
                guest gst1 = new guest();

                //Filling the Properties of guest
                gst1.FstName = txtForeName.Text;
                gst1.SndName = txtSurname.Text;
                gst1.PassPortNum = txtPPN.Text;

                string fstnametester = "";
                string sndnametester = "";
                string PPNTest = "";
                int int2age = 0;

                if (gst1.FstName != null)
                {
                    fstnametester = gst1.FstName;
                }

                if (gst1.SndName != null)
                {
                    sndnametester = gst1.SndName;
                }

                if (gst1.PassPortNum != null)
                    PPNTest = gst1.PassPortNum;

                int.TryParse(txtAge.Text, out int2age);
                gst1.Age = int2age;

                if (PPNTest.Length != 0 && PPNTest.Length < 11 && fstnametester.Trim() != "" && fstnametester.Trim() != null && sndnametester.Trim() != "" && sndnametester.Trim() != null && int2age < 101 && int2age > 0)
                {
                    gst1.GuestRefNum = guestAcc;
                    //Filling the properties of guest
                    gst1.CustomerRefNum = custnum.CustRefNum;
                    gst1.BookingRefNum = refnum.CurRefNum;
                    MessageBox.Show(gst1.displayGuest());

                    //add current guest to list
                    MainWindow.gstl.Add(gst1);

                    //add one to the accumulator and print it worked.
                    MessageBox.Show("**Guest #" + (guestAcc + 1) + " Added.**");
                    txtForeName.Text = "";
                    txtSurname.Text = "";
                    txtPPN.Text = "";
                    txtAge.Text = "";
                    guestAcc++;
                }
            }
            else
            {
                MessageBox.Show("**All guests added**");
                MainWindow MainWindowInst = new MainWindow();
                MainWindowInst.Show();
                Close();
            }
            
                
        }

        //used for ammending existing guest
        private void btnApplyChanges_Click(object sender, RoutedEventArgs e)
        {
            //bool to check if string has been checked
            bool haschecked = false;
            int int2age;
            int.TryParse(txtAge.Text, out int2age);
            string PPNTest, fstnametester, sndnametester;
            PPNTest = txtForeName.Text;
            fstnametester = txtForeName.Text;
            sndnametester = txtSurname.Text;

            //if string can be parsed then show booking details, else error message
            int bktoview;
            int guesttoview;

            if (int.TryParse(lblBkToView.Content.ToString(), out bktoview) == true)
            {
                bktoview = bktoview - 1;
                if (int.TryParse(lblGuestToView.Content.ToString(), out guesttoview) == true)
                {
                    guesttoview = guesttoview - 2;
                    foreach (guest gst in MainWindow.gstl)
                    {
                        //if guest refnum is equal to the user input then update booking details
                        if (PPNTest.Length != 0 && PPNTest.Length < 11 && fstnametester.Trim() != "" && fstnametester.Trim() != null && sndnametester.Trim() != "" && sndnametester.Trim() != null && int2age < 101 && int2age > 0 && gst.BookingRefNum == (bktoview + 1) && gst.GuestRefNum == (guesttoview + 1))
                        {
                            //update booking details
                            gst.FstName = txtForeName.Text;
                            gst.SndName = txtSurname.Text;
                            gst.PassPortNum = txtPPN.Text;
                            gst.Age = int2age;

                            haschecked = true;

                            //return to main window
                            MessageBox.Show("**Guest Updated**");
                            MainWindow ReturnToMainInst = new MainWindow();
                            ReturnToMainInst.Show();
                            Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("**ERROR: Please input a valid integer into the guest reference box**");
                }             
            }
            else
            {
                MessageBox.Show("**ERROR: Please input a valid integer in the booking reference box**");
            }
            
            if (haschecked == false)
            {
                MessageBox.Show("**ERROR: One or more of the user inputs were incorrect**");
            }               
        }

        private void btnDeleteGuest_Click(object sender, RoutedEventArgs e)
        {
            int bktoview, guesttoview;
            if (int.TryParse(lblBkToView.Content.ToString(), out bktoview) == true)
            {
                if (int.TryParse(lblGuestToView.Content.ToString(), out guesttoview) == true)
                {
                    guesttoview = guesttoview - 1;
                    for (int i = 0; i < MainWindow.gstl.Count; i++)
                    {
                        //if refnum is equal to the user input then update booking details
                        if (MainWindow.gstl[i].BookingRefNum == bktoview && MainWindow.gstl[i].GuestRefNum == guesttoview)
                        {
                            //remove guest from list
                            MainWindow.gstl.Remove(MainWindow.gstl[i]);
                            MessageBox.Show("**Guest Removed**");

                            //return to mainwindow
                            MainWindow ReturnToMainInst = new MainWindow();
                            ReturnToMainInst.Show();
                            Close();
                        }
                    }
                }
                else
                    MessageBox.Show("**ERROR: Please input a valid integer into the guest reference box**");
            }
            else
                MessageBox.Show("**ERROR: Please input a valid integer into the booking reference box**");
        }

        
        private void btnNewGuest_Click(object sender, RoutedEventArgs e)
        {            
            int guestNum = cNumOfGuests;
            if (guestsLeft != 0)
            {
                //add a guest
                //declaring instance of guest
                guest gst1 = new guest();

                //Filling the Properties of guest
                gst1.FstName = txtForeName.Text;
                gst1.SndName = txtSurname.Text;
                gst1.PassPortNum = txtPPN.Text;

                string fstnametester = "";
                string sndnametester = "";
                string PPNTest = "";
                int int2age = 0;

                if (gst1.FstName != null)
                {
                    fstnametester = gst1.FstName;
                }

                if (gst1.SndName != null)
                {
                    sndnametester = gst1.SndName;
                }

                if (gst1.PassPortNum != null)
                    PPNTest = gst1.PassPortNum;

                int.TryParse(txtAge.Text, out int2age);
                gst1.Age = int2age;

                if (PPNTest.Length != 0 && PPNTest.Length < 11 && fstnametester.Trim() != "" && fstnametester.Trim() != null && sndnametester.Trim() != "" && sndnametester.Trim() != null && int2age < 101 && int2age > 0)
                {
                    guestNum++;
                    gst1.GuestRefNum = cNumOfGuests;
                    //Filling the properties of guest
                    gst1.CustomerRefNum = custnum.CustRefNum;
                    gst1.BookingRefNum = refnum.CurRefNum;
                    MessageBox.Show(gst1.displayGuest());

                    //add current guest to list
                    MainWindow.gstl.Add(gst1);

                    //add one to the accumulator and print it worked.
                    MessageBox.Show("**Guest #" + (guestNum) + " Added.**");
                    txtForeName.Text = "";
                    txtSurname.Text = "";
                    txtPPN.Text = "";
                    txtAge.Text = "";
                    guestsLeft--;
                    cNumOfGuests++;
                }
            }
            else
            {
                //return to main window
                MessageBox.Show("**All guests added**");
                MainWindow MainWindowInst = new MainWindow();
                MainWindowInst.Show();
                Close();
            }            
        }
    }
}
