﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Author: Michael Paxton (40205149)
 * Class Purpose: Store information about extras of the booking
 * Last augmented: 04/12/16
 * Design Patterns: None
 */

namespace _40205149.cw2
{
    public class extras : CarHireInfo
    {
        //private properties
        private bool _evemeal, _breakfast, _carhire;
        private string _evemealinfo, _breakfastinfo;
        private int _bookingref;

        //public properties
        public bool eveMeal
        {
            get { return _evemeal; }
            set { _evemeal = value; }
        }

        public bool breakfast
        {
            get { return _breakfast; }
            set { _breakfast = value; }
        }
        
        public bool carHire
        {
            get { return _carhire; }
            set { _carhire = value; }
        }

        public string eveMealInfo
        {
            get { return _evemealinfo; }
            set { _evemealinfo = value; }
        }

        public string breakfastInfo
        {
            get { return _breakfastinfo; }
            set { _breakfastinfo = value; }
        }

        public int bookingRef
        {
            get { return _bookingref; }
            set { _bookingref = value; }
        }

    }
}
