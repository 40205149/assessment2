﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /*
     * Author: Michael Paxton (40205149)
     * Class Purpose: Used for inputting, ammending and deleting information about extras
     * Last augmented: 09/12/16
     * Design Patterns: None
     */
    public partial class NewExtras : Window
    {

        //declare an instance of booking reference number singleton
        refnumsingleton refnum = refnumsingleton.refInstance;

        //used for inputting new extras
        public NewExtras()
        {
            InitializeComponent();

            //display the appropriate button
            btnNewGuest.Visibility = Visibility.Visible;
            btnApplyChanges.Visibility = Visibility.Hidden;
            lblBkRefOut.Visibility = Visibility.Hidden;
        }

        //used for viewing extras
        public NewExtras(int bkRefNum, bool eveMeal, string eveMealInfo, bool breakfast, string breakfastInfo, bool carhire)
        {
            InitializeComponent();

            //display the appropriate button
            btnNewGuest.Visibility = Visibility.Hidden;
            btnApplyChanges.Visibility = Visibility.Visible;
            lblBkRefOut.Visibility = Visibility.Visible;

            //display properties
            lblBkRefOut.Content = bkRefNum.ToString();
            if (eveMeal == true)
            {
                chkEveMeal.IsChecked = true;
                txtEveMealInfo.Text = eveMealInfo;
            }
            else if(eveMeal == false)
            {
                chkEveMeal.IsChecked = false;
            }
            if (breakfast == true)
            {
                chkBreakfast.IsChecked = true;
                txtBreakfastInfo.Text = breakfastInfo;
            }
            else if (breakfast == false)
            {
                chkBreakfast.IsChecked = false;
            }
            if (carhire == true)
            {
                chkCarHire.IsChecked = true;               
            }
            else if (carhire == false)
            {
                chkCarHire.IsChecked = false;
            }
            
        }

        //used for inputting new extras and then moving onto new guests
        private void btnNewGuest_Click(object sender, RoutedEventArgs e)
        {
            //creating instance of extras
            extras ext1 = new extras();

            //filling the properties of extras
            ext1.bookingRef = refnum.CurRefNum;
            if (chkEveMeal.IsChecked == true)
            {
                ext1.eveMeal = true;
            }               
            else
            {
                ext1.eveMeal = false;
            }  
            if (chkBreakfast.IsChecked == true)
            {
                ext1.breakfast = true;
            }                
            else
            {
                ext1.breakfast = false;
            }                
            if (chkCarHire.IsChecked == true)
            {               
                ext1.carHire = true;
                CarHire carHireInst = new CarHire();
                carHireInst.ShowDialog();
            }                
            else
            {
                ext1.carHire = false;
            }             
            if (ext1.eveMeal == true)
            {
                ext1.eveMealInfo = txtEveMealInfo.Text;
            }                
            if (ext1.breakfast == true)
            {
                ext1.breakfastInfo = txtBreakfastInfo.Text;
            }
                
            //add current extras to list
            MainWindow.extrl.Add(ext1);

            NewGuest newGuestInst = new NewGuest();
            newGuestInst.Show();
            Close();
        }

        //used for ammending extras
        private void btnApplyChanges_Click(object sender, RoutedEventArgs e)
        {
            //ammending the properties of extras
            int bkIndex = (Int32.Parse(lblBkRefOut.Content.ToString()) - 1);

            if (chkEveMeal.IsChecked == true)
            {
                MainWindow.extrl[bkIndex].eveMeal = true;
            }
            else if (chkEveMeal.IsChecked == false)
            {
                MainWindow.extrl[bkIndex].eveMeal = false;
            }
            if (chkBreakfast.IsChecked == true)
            {
                MainWindow.extrl[bkIndex].breakfast = true;
            }
            else if (chkBreakfast.IsChecked == false)
            {
                MainWindow.extrl[bkIndex].breakfast = false;
            }
            if (chkCarHire.IsChecked == true)
            {
                MainWindow.extrl[bkIndex].carHire = true;

                bool haschecked = false;
                for (int i = 0; i < MainWindow.chil.Count();i++ )
                {
                    if (MainWindow.chil[i].bookingRef == bkIndex + 1)
                    {
                        MessageBox.Show("**CHI exists**");
                        haschecked = true;
                        CarHire carHireInst = new CarHire(i, MainWindow.chil[i].StartDate, MainWindow.chil[i].EndDate, MainWindow.chil[i].FstName, MainWindow.chil[i].Surname);
                        carHireInst.ShowDialog();
                    }                    
                }
                if (haschecked == false)
                {
                    MessageBox.Show("CHI doesn't exist");
                    CarHire carHireInst = new CarHire();
                    carHireInst.ShowDialog();
                }                    
            }
            else if (chkCarHire.IsChecked == false)
            {
                MainWindow.extrl[bkIndex].carHire = false;
            }
            if (MainWindow.extrl[bkIndex].eveMeal == true)
            {
                MainWindow.extrl[bkIndex].eveMealInfo = txtEveMealInfo.Text;
            }
            if (MainWindow.extrl[bkIndex].breakfast == true)
            {
                MainWindow.extrl[bkIndex].breakfastInfo = txtBreakfastInfo.Text;
            }

            //return to main window
            MessageBox.Show("**Extras updated**");
            MainWindow ReturnToMainInst = new MainWindow();
            ReturnToMainInst.Show();
            Close();
        } 

    }
}
