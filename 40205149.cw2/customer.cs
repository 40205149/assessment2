﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/*
 * Author: Michael Paxton (40205149)
 * Class Purpose: Store information about the customer.
 * Last augmented: 05/12/16
 * Design Patterns: None.
 */

namespace _40205149.cw2
{
    public class customer
    {
        //private properties
        private string _fstname, _sndname, _address;
        private int _custrefnum;

        //public properties
        public string FstName
        {
            get { return _fstname; }
            set
            {
                try
                {
                    if (value.Trim() == "" || value.Trim() == null)
                        throw new ArgumentException("**No first name found**");
                    _fstname = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public string SndName
        {
            get { return _sndname; }
            set
            {
                try
                {
                    if (value.Trim() == "" || value.Trim() == null)
                        throw new ArgumentException("**No surname found**");
                    _sndname = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public string Address
        {
            get { return _address; }
            set 
            { 
                try
                {
                    if (value == "")
                        throw new ArgumentException("**No address found**");
                    _address = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
                 
            }
        }
        
        public int CustRefNum
        {
            get { return _custrefnum; }
            set { _custrefnum = value; }
        }

        public string displayCustomer()
        {
            return ("Customer: " + _fstname + " " + _sndname + ". With the reference number of: " + _custrefnum + ". lives at the address: " + _address + ".");
        }

    }
}
