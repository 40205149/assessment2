﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Author: Michael Paxton (40205149)
 * Class Purpose: Singleton used to create an incremental integer to be used as the Customer reference number
 * Last augmented: 30/11/16
 * Design Patterns: Singleton.
 */

namespace _40205149.cw2
{
    class custrefnumsingleton
    {
         //declare an instance of refnumsingleton
        private static custrefnumsingleton custinstance;
        private custrefnumsingleton() { }

        //create an instance of refnumsingleton
        public static custrefnumsingleton custInstance
        {
            get
            {
                // If being called for the first time, create refnum object.
                if (custinstance == null)                                 
                {
                    custinstance = new custrefnumsingleton();
                }
                return custinstance;
            }
        }

        //properties
        private int _custrefnum = 0;
        public int CustRefNum 
        {
            get { return _custrefnum; }
            set { _custrefnum = value; }
        }

        //Methods
        public int getNextRefNum()
        {
            _custrefnum = _custrefnum + 1;
            return _custrefnum;
        }

    }
}
