﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /*
     * Author: Michael Paxton (40205149)
     * Class Purpose: Used for inputting information about hiring cars 
     * Last augmented: 08/12/16
     * Design Patterns: None
     */
    public partial class CarHire : Window
    {
        public CarHire()
        {
            InitializeComponent();

            //display the appropriate button
            btnConfirm.Visibility = Visibility.Visible;
            btnApplyChanges.Visibility = Visibility.Hidden;
            lblBkRefOut.Visibility = Visibility.Hidden;
        }

        public CarHire(int bkIndex, DateTime startDate, DateTime endDate, string forename, string surname)
        {
            InitializeComponent();

            //display the appropriate button
            btnConfirm.Visibility = Visibility.Hidden;
            btnApplyChanges.Visibility = Visibility.Visible;
            lblBkRefOut.Visibility = Visibility.Visible;

            lblBkRefOut.Content = (bkIndex + 1).ToString();
            dtpStartDate.SelectedDate = startDate;
            dtpEndDate.SelectedDate = endDate;
            txtDriveFst.Text = forename;
            txtDriveSur.Text = surname;
        }

        //used to set the values in the tools into carhireinfo
        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            //create new instance of carhireinfo
            CarHireInfo carinf1 = new CarHireInfo();

            int currefnum = refnumsingleton.refInstance.CurRefNum - 1;

            //fill properties of carinf1
            DateTime bkarrival = new DateTime();
            bkarrival = MainWindow.bkl[currefnum].ArrivalDate;
            DateTime bkdepart = new DateTime();
            bkdepart = MainWindow.bkl[currefnum].DepartureDate;
            carinf1.FstName = txtDriveFst.Text;
            carinf1.Surname = txtDriveSur.Text;
            
            carinf1.StartDate = carinf1.datetovar(dtpStartDate.SelectedDate.ToString());
            carinf1.EndDate = carinf1.datetovar(dtpEndDate.SelectedDate.ToString());

            if (carinf1.StartDate < bkarrival || carinf1.StartDate > bkdepart || carinf1.EndDate < carinf1.StartDate || carinf1.EndDate > bkdepart || carinf1.EndDate < bkarrival || carinf1.FstName == "" || carinf1.FstName == null || carinf1.Surname == null || carinf1.Surname == "")
            {
            }
            else
            {                
                carinf1.bookingRef = refnumsingleton.refInstance.CurRefNum;

                //Display update alert
                MessageBox.Show("**Car Hire Information Updated**");

                MainWindow.chil.Add(carinf1);
                Close();
            }
                     
        }

        //used for ammending carhireinfo
        private void btnApplyChanges_Click(object sender, RoutedEventArgs e)
        {
            int bkIndex = (Int32.Parse(lblBkRefOut.Content.ToString()) - 1);

            //fill properties of carinf1
            DateTime bkarrival = new DateTime();
            bkarrival = MainWindow.bkl[bkIndex].ArrivalDate;
            DateTime bkdepart = new DateTime();
            bkdepart = MainWindow.bkl[bkIndex].DepartureDate;

            MainWindow.chil[bkIndex].StartDate = MainWindow.chil[bkIndex].datetovar(dtpStartDate.SelectedDate.ToString());
            MainWindow.chil[bkIndex].EndDate = MainWindow.chil[bkIndex].datetovar(dtpEndDate.SelectedDate.ToString());
            if (MainWindow.chil[bkIndex].StartDate < bkarrival || MainWindow.chil[bkIndex].StartDate > bkdepart || MainWindow.chil[bkIndex].EndDate < MainWindow.chil[bkIndex].StartDate || MainWindow.chil[bkIndex].EndDate > bkdepart || MainWindow.chil[bkIndex].EndDate < bkarrival)
            {
                MessageBox.Show("**ERROR: date invalid**");
            }
            else
            {
                MainWindow.chil[bkIndex].FstName = txtDriveFst.Text;
                MainWindow.chil[bkIndex].Surname = txtDriveSur.Text;

                //Display update alert
                MessageBox.Show("**Car Hire Information Updated**");
                Close();
            }
        }
    }
}
