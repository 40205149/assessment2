﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /*
     * Author: Michael Paxton (40205149)
     * Class Purpose: Used for inputting, ammending and deleting information about customers
     * Last augmented: 08/12/16
     * Design Patterns: None
     */
    public partial class NewCust : Window
    {

        //declare an instance of customer reference number singleton
        custrefnumsingleton custnum = custrefnumsingleton.custInstance;

        //used for inputting new customer
        public NewCust()
        {
            InitializeComponent(); 

            //display the appropriate tools
            btnNewBooking.Visibility = Visibility.Visible;
            btnApplyChanges.Visibility = Visibility.Hidden;
            btnDeleteCust.Visibility = Visibility.Hidden;           
        }

        //used for viewing customer
        public NewCust(int custrefnum, string forename, string surname, string address)
        {
            InitializeComponent();

            //display the appropriate tools
            btnNewBooking.Visibility = Visibility.Hidden;
            btnApplyChanges.Visibility = Visibility.Visible;
            btnDeleteCust.Visibility = Visibility.Visible;

            //set tools to display customer information
            lblCurCustOut.Content = custrefnum.ToString();
            txtForename.Text = forename;
            txtSurname.Text = surname;
            txtAddress.Text = address;
            MessageBox.Show("**Booking loaded**");
        }

        //used for creating a new customer, then moving on to adding a new booking
        private void btnNewBooking_Click(object sender, RoutedEventArgs e)
        {
            //Creating an instance of customer
            customer cst1 = new customer();

            //filling the properties of customer
            cst1.FstName = "" + txtForename.Text;
            cst1.SndName = "" + txtSurname.Text;
            cst1.Address = "" + txtAddress.Text;

            string fNameTest = "" + txtForename.Text;
            string sNameTest = "" + txtSurname.Text;

            //add current customer to list if all the values are filled
            if (fNameTest.Trim() != "" && sNameTest.Trim() != "" && cst1.Address != "" && cst1.Address != null && fNameTest.Trim() != null && sNameTest.Trim() != null)
            {
                //if they are filled that is when the next refnum should be assigned
                cst1.CustRefNum = custnum.getNextRefNum();

                //add customer to list
                MainWindow.cstl.Add(cst1);

                //testing
                MessageBox.Show(cst1.displayCustomer());

                MessageBox.Show("**Customer added to list**");           
                NewBooking newBkInst = new NewBooking();
                newBkInst.Show();
                Close();
            }            
        }

        //used for applying changes to customer
        private void btnApplyChanges_Click(object sender, RoutedEventArgs e)
        {
            //create an instance of customer
            customer cst1 = new customer();

            //filling the properties of customer
            cst1.FstName = "" + txtForename.Text;
            cst1.SndName = "" + txtSurname.Text;
            cst1.Address = "" + txtAddress.Text;

            string fNameTest = "" + txtForename.Text;
            string sNameTest = "" + txtSurname.Text;
            int custIndex = (int.Parse(lblCurCustOut.Content.ToString()) - 1);

            //add current customer to list if all the values are filled
            if (fNameTest.Trim() != "" && sNameTest.Trim() != "" && cst1.Address != "" && cst1.Address != null && fNameTest.Trim() != null && sNameTest.Trim() != null)
            {
                MainWindow.cstl[custIndex].FstName = txtForename.Text;
                MainWindow.cstl[custIndex].SndName = txtSurname.Text;
                MainWindow.cstl[custIndex].Address = txtAddress.Text;
                MessageBox.Show("**Changes applied**");

                //return to mainwindow
                MainWindow ReturnToMainInst = new MainWindow();
                ReturnToMainInst.Show();
                Close();
            }
        }

        private void btnDeleteCust_Click(object sender, RoutedEventArgs e)
        {
            bool haschecked = false;
            int custtoview;
            if (int.TryParse(lblCurCustOut.Content.ToString(), out custtoview) == true)
            {
                for (int c = 0; c < MainWindow.cstl.Count; c++)
                {
                    //if refnum is equal to the user input then update booking details
                    if (MainWindow.cstl[c].CustRefNum == custtoview)
                    {
                        //before deleting anything, check to see customer has any bookings, extras, or guests
                        foreach(booking bk in MainWindow.bkl)
                        {
                            if (bk.CustomerRefNum == custtoview)
                            {
                                haschecked = true;
                            }
                        }                                         
                    }
                }                
            }
            else
                MessageBox.Show("**ERROR: Please input a valid integer into the booking reference box**");
            if (haschecked == false)
            {
                //remove customer from list
                MainWindow.cstl.Remove(MainWindow.cstl[custtoview - 1]);
                MessageBox.Show("**Customer Removed**");

                //return to mainwindow
                MainWindow ReturnToMainInst = new MainWindow();
                ReturnToMainInst.Show();
                Close();
            }
            else
            {
                //keep customer in list
                MessageBox.Show("**ERROR: Customer cannot be removed due to booking existing**");

                //return to mainwindow
                MainWindow ReturnToMainInst = new MainWindow();
                ReturnToMainInst.Show();
                Close();
            }                                                         
        }


    }
}
