﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/*
 * Author: Michael Paxton (40205149)
 * Class Purpose: Stores properties and methods for bookings
 * Last augmented: 08/12/16
 * Design Patterns: None.
 */

namespace _40205149.cw2
{
    public class booking 
    {
        //Private Properties
        private DateTime _arrivaldate = new DateTime();
        private DateTime _departuredate = new DateTime();
        private int _refnum, _customerrefnum, _noofguests;
        
        refnumsingleton refinstance = refnumsingleton.refInstance;

        //Public Properties
        public DateTime ArrivalDate
        {
            get { return _arrivaldate; }
            set 
            {
                try
                {
                    if (value < DateTime.Today)
                        throw new ArgumentException("**ERROR: Please enter a valid date.**");
                    _arrivaldate = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }
       
        public DateTime DepartureDate
        {
            get { return _departuredate; }
            set
            {
                try
                {
                    if (value < DateTime.Today)
                        throw new ArgumentException("**ERROR: Please enter a valid date.**");
                    if (value <= _arrivaldate)
                        throw new ArgumentException("**ERROR: Please enter a departure date later than the arrival date.**");
                    _departuredate = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        public int RefNum
        {
            get { return _refnum; }
            set { _refnum = value; }
        }

        public int CustomerRefNum
        {
            get { return _customerrefnum; }
            set { _customerrefnum = value; }
        }

        public int NoOfGuests
        {
            get { return _noofguests; }
            set 
            {
                try
                {
                    if (value < 1)
                        throw new ArgumentException("**ERROR: Please enter more guests (between 1-4)**");
                    if (value > 4)
                        throw new ArgumentException("**ERROR: Please enter less guests (between 1-4)**");
                    _noofguests = value;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        //Methods
        public string displayBooking()
        {
            string output = "Booking placed by Customer #" + _customerrefnum + ". Arrival Date: " + _arrivaldate + " Departure Date: " + _departuredate + " Reference Number: " + _refnum;
            return output;
        }

        public double[] getCost(List<guest> gstl, List<extras> extrl, List<CarHireInfo> chil)
        {
            //Declaring the output and nights stayed variable
            double total = 0.00;
            double nights = 0;
            double nightlycost = 0.00;
            double extrascost = 0.00;
            double carhirecost = 0.00;
            bool extraschecked = false;
            int noofguests = 0;

            //Calculate the number of nights the guests stayed for this booking
            nights = (_departuredate - _arrivaldate).TotalDays;

            for (int i = 0; i < gstl.Count; i++)
            {
                //If this bookings refnum is equal to that of the booking refnum assigned to the guest, then accumulate cost
                if (_refnum == gstl[i].BookingRefNum)
                {
                    if (gstl[i].Age < 18)
                    {
                        nightlycost = nightlycost + 30.00;
                    }
                    else
                    {
                        nightlycost = nightlycost + 50.00;
                    }
                    if (extraschecked == false)
                    {
                        if (extrl[i].eveMeal == true)
                        {
                            extrascost = extrascost + 15.00;
                        }
                        if (extrl[i].breakfast == true)
                        {
                            extrascost = extrascost + 5.00;
                        }
                        if (extrl[i].carHire == true)
                        {
                            foreach (CarHireInfo chi in chil)
                            {
                                if (chi.bookingRef == _refnum )
                                {
                                    carhirecost = chi.getDays(chi.StartDate, chi.EndDate) * 50.00;
                                }
                            }                         
                        }
                        extraschecked = true;
                    }  
                }
                noofguests = noofguests + 1;
            }
            total = total + (nightlycost * nights) + (extrascost * noofguests * nights) + carhirecost;
             
            double[] outputs = {nightlycost, extrascost, carhirecost,  total};
            return outputs;
        }

        public DateTime datetovar(string dat)
        {
            if (dat.Equals(""))
            {
                return new DateTime();
            }
            else
            {
                return new DateTime(Int32.Parse(dat.Substring(6, 4)),
                    Int32.Parse(dat.Substring(3, 2)),
                    Int32.Parse(dat.Substring(0, 2)));
            }
        }
        
    }
}
