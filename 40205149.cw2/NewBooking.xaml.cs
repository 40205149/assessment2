﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /*
     * Author: Michael Paxton (40205149)
     * Class Purpose: Used for inputting, ammending and deleting information about bookings
     * Last augmented: 08/12/16
     * Design Patterns: None
     */
    public partial class NewBooking : Window
    {

        //declare instances of customer reference number and booking reference number singleton
        custrefnumsingleton custnum = custrefnumsingleton.custInstance;
        refnumsingleton refnum = refnumsingleton.refInstance;       

        //used for inputting new booking
        public NewBooking()
        {
            InitializeComponent();

            //display the appropriate button
            btnAddExtras.Visibility = Visibility.Visible;
            btnAddBooking.Visibility = Visibility.Hidden;
            btnApplyChanges.Visibility = Visibility.Hidden;
            btnDeleteBooking.Visibility = Visibility.Hidden;
            btnGetCost.Visibility = Visibility.Hidden;
            lblBookingToView.Visibility = Visibility.Hidden;          
        }

        //used for viewing booking
        public NewBooking(int bkIndex, DateTime dateArrival, DateTime dateDeparture, int numOfGuests)
        {
            InitializeComponent();

            //display the appropriate button
            btnAddExtras.Visibility = Visibility.Hidden;
            btnAddBooking.Visibility = Visibility.Visible;
            btnApplyChanges.Visibility = Visibility.Visible;
            btnDeleteBooking.Visibility = Visibility.Visible;
            btnGetCost.Visibility = Visibility.Visible;
            lblBookingToView.Visibility = Visibility.Visible;
            
            //set tools to display booking information
            dtpArrival.SelectedDate = dateArrival;
            dtpDeparture.SelectedDate = dateDeparture;
            txtNoOfGuests.Text = numOfGuests.ToString();
            lblBookingToView.Content = bkIndex.ToString();
            MessageBox.Show("**Booking loaded**");
        }

       //used for creating a new booking, then moving on to adding a new set of extras
        private void btnAddExtras_Click(object sender, RoutedEventArgs e)
        {
            //create instance of booking
            booking bk1 = new booking();

            //filling the properties of booking
            bk1.ArrivalDate = bk1.datetovar(dtpArrival.SelectedDate.ToString());
            bk1.DepartureDate = bk1.datetovar(dtpDeparture.SelectedDate.ToString());
            int numofguests;
            if (int.TryParse(txtNoOfGuests.Text, out numofguests) == true)
            {
                bk1.NoOfGuests = numofguests;
                //add current booking to list if all values are filled
                if (dtpArrival.SelectedDate != null && dtpDeparture.SelectedDate != null
                    && dtpDeparture.SelectedDate > dtpArrival.SelectedDate && dtpArrival.SelectedDate >= DateTime.Today
                    && numofguests <= 4 && numofguests > 0)
                {

                    //if they are filled that is when the next refnum should be assigned
                    bk1.RefNum = refnum.getNextRefNum();
                    bk1.CustomerRefNum = custnum.CustRefNum;

                    //add current booking to list
                    MainWindow.bkl.Add(bk1);

                    //testing 
                    MessageBox.Show("" + bk1.displayBooking());

                    //request user to enter extras details
                    NewExtras newExtrInst = new NewExtras();
                    newExtrInst.Show();
                    Close();
                }      
            }
            else
            {
                MessageBox.Show("**ERROR: Please enter a valid integer for number of guests**");
            }
        }

        //used for adding a new booking
        private void btnAddBooking_Click(object sender, RoutedEventArgs e)
        {
            //create instance of booking
            booking bk1 = new booking();

            //filling the properties of booking
            bk1.ArrivalDate = bk1.datetovar(dtpArrival.SelectedDate.ToString());
            bk1.DepartureDate = bk1.datetovar(dtpDeparture.SelectedDate.ToString());
            int numofguests;
            if (int.TryParse(txtNoOfGuests.Text, out numofguests) == true)
            {
                bk1.NoOfGuests = numofguests;
                //add current booking to list if all values are filled
                if (dtpArrival.SelectedDate != null && dtpDeparture.SelectedDate != null
                    && dtpDeparture.SelectedDate > dtpArrival.SelectedDate && dtpArrival.SelectedDate >= DateTime.Today
                    && numofguests <= 4 && numofguests > 0)
                {

                    //if they are filled that is when the next refnum should be assigned
                    bk1.RefNum = refnum.getNextRefNum();
                    bk1.CustomerRefNum = custnum.CustRefNum;

                    //add current booking to list
                    MainWindow.bkl.Add(bk1);

                    //testing 
                    MessageBox.Show("" + bk1.displayBooking());

                    //request user to enter extras details
                    NewExtras newExtrInst = new NewExtras();
                    newExtrInst.Show();
                    Close();
                }
            }
            else
            {
                MessageBox.Show("**ERROR: Please enter a valid integer for number of guests**");
            }
        }

        //used for applying changes to booking
        private void btnApplyChanges_Click(object sender, RoutedEventArgs e)
        {
        //create an instance of booking
        booking bk1 = new booking();

        //filling the properties of customer
            
            bk1.ArrivalDate = bk1.datetovar(dtpArrival.SelectedDate.ToString());
            bk1.DepartureDate = bk1.datetovar(dtpDeparture.SelectedDate.ToString());
            int bkIndex = (int.Parse(lblBookingToView.Content.ToString()) - 1);

            int curNumOfGuests = MainWindow.bkl[bkIndex].NoOfGuests;
            int newNumOfGuests;
            if (int.TryParse(txtNoOfGuests.Text, out newNumOfGuests) == true)
            {
                if (curNumOfGuests == newNumOfGuests)
                {
                    //do it normally
                    //add current booking to list if all the values are filled
                    if (dtpArrival.SelectedDate != null && dtpDeparture.SelectedDate != null && dtpDeparture.SelectedDate > dtpArrival.SelectedDate && dtpArrival.SelectedDate >= DateTime.Today)
                    {
                        MainWindow.bkl[bkIndex].ArrivalDate = MainWindow.bkl[bkIndex].datetovar(dtpArrival.SelectedDate.ToString());
                        MainWindow.bkl[bkIndex].DepartureDate = MainWindow.bkl[bkIndex].datetovar(dtpDeparture.SelectedDate.ToString());
                        MessageBox.Show("**Changes applied**");

                        //since returning to mainwindow, hide apply changes button
                        btnApplyChanges.Visibility = Visibility.Hidden;
                        MainWindow ReturnToMainInst = new MainWindow();
                        ReturnToMainInst.Show();
                        Close();
                    }
                }
                //if the new number of guests is bigger than the old one add a new guest and do all normal changes
                else if (newNumOfGuests > curNumOfGuests && newNumOfGuests < 5 && newNumOfGuests > 0)
                {
                    if (dtpArrival.SelectedDate != null && dtpDeparture.SelectedDate != null && dtpDeparture.SelectedDate > dtpArrival.SelectedDate && dtpArrival.SelectedDate >= DateTime.Today)
                    {
                        MainWindow.bkl[bkIndex].ArrivalDate = MainWindow.bkl[bkIndex].datetovar(dtpArrival.SelectedDate.ToString());
                        MainWindow.bkl[bkIndex].DepartureDate = MainWindow.bkl[bkIndex].datetovar(dtpDeparture.SelectedDate.ToString());
                        MessageBox.Show("**Changes applied**");

                        //since returning to mainwindow, hide apply changes button
                        btnApplyChanges.Visibility = Visibility.Hidden;
                        NewGuest newGuestInst = new NewGuest(curNumOfGuests, newNumOfGuests);
                        newGuestInst.Show();
                        Close();
                    }
                }
                else
                {
                    MessageBox.Show("**ERROR: to remove guests please view guest and press delete**");
                }            
            }
            else
            {
                MessageBox.Show("**ERROR: Please enter a valid integer for number of guests**");
            }
        }
        
        //used for deleting a booking
        private void btnDeleteBooking_Click(object sender, RoutedEventArgs e)
        {
            bool haschecked = false;
            int bookingtoview = 0;
            //if string can be parsed then show booking details, else if text box is null, empty or not an integer then error message           
            if (int.TryParse(lblBookingToView.Content.ToString(), out bookingtoview) == true)
            {
                foreach (booking bk in MainWindow.bkl)
                {
                    if (bk.RefNum == bookingtoview)
                    {
                        //before removing anything, check if booking has any guests attached
                        foreach (guest gst in MainWindow.gstl)
                        {
                            if (gst.BookingRefNum == bookingtoview)
                            {
                                haschecked = true; 
                            }
                        }                          
                    }
                }
                    
            }
            else
            {
                MessageBox.Show("**ERROR: Please input a valid integer into the booking reference box**");
            }                    
             
            if (haschecked == false)
            {
                //remove booking, extras, and carhire from respective lists

                foreach (extras ext in MainWindow.extrl.ToList())
                {
                    if (ext.bookingRef == bookingtoview)
                    {
                        MainWindow.extrl.Remove(ext);
                    }
                } 

                foreach (CarHireInfo chi in MainWindow.chil.ToList())
                {
                    if (chi.bookingRef == bookingtoview)
                    {
                        MainWindow.chil.Remove(chi);
                    }
                }
     
                MainWindow.bkl.Remove(MainWindow.bkl[bookingtoview - 2]);

                MessageBox.Show("**Booking Removed**");

                //return to mainwindow
                MainWindow ReturnToMainInst = new MainWindow();
                ReturnToMainInst.Show();
                Close();
            }
            else
            {
                //keep booking in list
                MessageBox.Show("**Booking cannot be removed due to guest existing**");

                //return to mainwindow
                MainWindow ReturnToMainInst = new MainWindow();
                ReturnToMainInst.Show();
                Close();
            }
        }

        //prints an invoice with the cost of the booking
        private void btnGetCost_Click(object sender, RoutedEventArgs e)
        {

                int bookingtogetcost;
                if (int.TryParse(lblBookingToView.Content.ToString(), out bookingtogetcost) == true)
                    foreach (booking bk in MainWindow.bkl)
                    {
                        if (bk.RefNum == bookingtogetcost)
                        {
                            double[] outputs = bk.getCost(MainWindow.gstl, MainWindow.extrl, MainWindow.chil);
                            Invoice invOutput = new Invoice(outputs);
                            invOutput.ShowDialog();
                        }
                    }
                else
                    MessageBox.Show("**ERROR: Please input a valid integer into the GetCost textbox**");
        }

    }
}
