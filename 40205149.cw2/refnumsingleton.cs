﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Author: Michael Paxton (40205149)
 * Class Purpose: Singleton used to create an incremental integer to be used as the booking reference number
 * Last augmented: 30/11/16
 * Design Patterns: Singleton.
 */

namespace _40205149.cw2
{
    public class refnumsingleton
    {
        //declare an instance of refnumsingleton
        private static refnumsingleton refinstance;
        private refnumsingleton() { }

        //create an instance of refnumsingleton
        public static refnumsingleton refInstance
        {
            get
            {
                // If being called for the first time, create refnum object.
                if (refinstance == null)                                 
                {
                    refinstance = new refnumsingleton();
                }
                return refinstance;
            }
        }

        //properties
        private int _currefnum = 0;
        public int CurRefNum 
        {
            get { return _currefnum; }
            set { _currefnum = value; }
        }

        //Methods
        public int getNextRefNum()
        {
            _currefnum = _currefnum + 1;
            return _currefnum;
        }

    }
}

        

            

        

 
