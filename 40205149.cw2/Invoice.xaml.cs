﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _40205149.cw2
{
    /*
     * Author: Michael Paxton (40205149)
     * Class Purpose: Used for outputting invoice about entire booking 
     * Last augmented: 08/12/16
     * Design Patterns: None
     */
    public partial class Invoice : Window
    {
        public Invoice()
        {
            InitializeComponent();
        }

        public Invoice(double[] outputs)
        {
            InitializeComponent();
            txtRoomPrice.Text = "£" + outputs[0].ToString();
            txtExtrasPrice.Text = "£" + outputs[1].ToString();
            txtCarHirePrice.Text = "£" + outputs[2].ToString();
            txtTotalPrice.Text = "£" + outputs[3].ToString();        
        }
    }
}
